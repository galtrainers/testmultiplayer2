// Fill out your copyright notice in the Description page of Project Settings.


#include "GetExeParams.h"

TMap<FString, FString>  UGetExeParams::GetExeParams()
{

	// to change later not to hard-coded  

	//FString args_string = FCommandLine::Get(); // return after  
	//FString args_string = "map=Data_L20_fbx";// return after 
	TArray< FString>ArgsTempArr;
	//TArray< FString>ArgsArr; // return after 
	FString ParamsHardCoded = "map=TilesWithCSV ip=10.0.0.8 IsServer=true";
	TMap<FString, FString> Args;
	ParamsHardCoded.ParseIntoArray(ArgsTempArr, TEXT(" "));
	//args_string.ParseIntoArray(ArgsTempArr, TEXT(" ")); // return after 
	for (int i = 0;i < ArgsTempArr.Num(); i++)
	{
		TArray< FString> KeyValArg;
		ArgsTempArr[i].ParseIntoArray(KeyValArg, TEXT("="));
		Args.Add(KeyValArg[0]);
		Args[KeyValArg[0]] = KeyValArg[1];
		//GEngine->AddOnScreenDebugMessage(-1, 200, FColor::Green, FString::Printf(TEXT("key:%s value:%s \n "), *KeyValArg[0], *KeyValArg[1]));
	}
	//ArgsArr = FParse::Value(TEXT("="), my_var)
	return Args;
}