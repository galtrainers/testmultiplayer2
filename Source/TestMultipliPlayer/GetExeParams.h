// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GetExeParams.generated.h"

/**
 * 
 */
UCLASS()
class TESTMULTIPLIPLAYER_API UGetExeParams : public UObject
{
	GENERATED_BODY()

	public:
		UFUNCTION(BlueprintCallable, Category = "SelectMaps")
		static TMap<FString, FString> GetExeParams();
};
