// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestMultipliPlayerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TESTMULTIPLIPLAYER_API ATestMultipliPlayerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
